<?php

/**
 * @file
 * Contains class BankingBICFormElement.
 */

/**
 * Tests the banking_bic form element.
 */
class BankingBICFormElement extends DrupalWebTestCase {

  /**
   * Implements DrupalTestCase::getInfo().
   */
  static function getInfo() {
    return array(
      'name' => 'banking_bic form element',
      'group' => 'Banking',
    );
  }

  /**
   * Overrides parent::setUp().
   */
  function setUp(array $modules = array()) {
    $this->profile = 'testing';
    parent::setUp($modules + array('banking_test'));
  }

  /**
   * Test validation.
   */
  function testValidation() {
    $path = 'banking_test_form_element_banking_bic';

    // Test valid values.
    $valid_form_state = 'DSBACNBXSHA';
    $valid_form_post = array(
      'banking_bic' => 'DSBACNBXSHA',
    );
    $this->drupalPost($path, $valid_form_post, t('Submit'));
    $this->assertUrl('user');
    $form_state = variable_get('banking_test_form_submit');
    $this->assertEqual($valid_form_state, $form_state['banking_bic']);

    // Test valid values.
    $valid_form_state = '12ABONL2U';
    $valid_form_post = array(
      'banking_bic' => '12ABONL2U',
    );
    $this->drupalPost($path, $valid_form_post, t('Submit'));
    // Test that the invalid element is the only element to be flagged.
    $this->assertFieldByXPath("//input[@name='banking_bic' and contains(@class, 'error')]");
    $this->assertNoFieldByXPath("//input[not(@name='banking_bic') and contains(@class, 'error')]");
  }
}

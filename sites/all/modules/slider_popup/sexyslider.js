// jQuery .toggle("fast") causes the .panels element to be visible/invisible
// jQuery .toggleClass("active") causes one or more class names to be toggled
// for each element in the matched set

(function ($) {
	$(document).ready(function(){
	  
    $(".trigger").click(function(){
          
          var bHeight = $(".trigger").height();
          var bWidth = $(".trigger-div").width();
          var tWidth = $(".trigger").width();
          var extraSpace = tWidth - bWidth;
          var marginRt = - (extraSpace + 40);
         
          var pTop = -(bHeight + 10);
          var pPadLeft = bWidth; 
          var pPadRight = bWidth;
          var pPadDefault = $('.panels').css("padding-top");
          
          var flyFromDirection = $(".panel-orient").text(); 
                
          $(".panels").css("top", pTop);
          
          if (flyFromDirection == 'left') {
              $(".panels").css("padding-left",pPadLeft); 
              $(".panels").css("padding-right",pPadDefault);
              $(".panels").css("left",0);
              $(".panels").css("right","auto");
              $(".panels").css("margin-left",20);
            }
          else {
              $(".panels").css("padding-right",pPadRight); 
              $(".panels").css("padding-left",pPadDefault);
              $(".panels").css("right",0);
              $(".panels").css("left","auto");
              $(".panels").css("margin-right",marginRt);
              
          }
          
          
          $(".panels").toggle("fast");
	        $(this).toggleClass("active");
	        return false;
	    });
	});
})(jQuery);

This module to add a sliding panel feature

Sliding_Popup uses a combination of JavaScript, JQuery and CSS3 to implement the
feature of a popup panel on the screen.  The first implementation will be for a
sliding block from the left.

The minimum required files for a module are slider_popup.admin.inc,
slider_popup.info and slider_popup.module.  I added a requirement in
the slider_popup.info for ckeditor, so that the text_format field can be WYSIWYG.

Author: Susan Fox (sfox2681 at gmail dot com)
Website: http://fox-custom-software.com


